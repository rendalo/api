# RENDALO - API v1

## Requirements

- Docker
- Node.js >= 8.12.0
- Yarn

## Setting Up

### Docker and MongoDB

```bash
docker run --name rendalo_db -d -p 27017:27017 -v HOST/PATH/WHERE/STORE/DB/DATA:/data/db mongo
```

### Install Dependencies

```bash
yarn
```

### Environment Variables

Create two files, `.env.dev` and `.env.test`, in the project root. Both files should have the address to the `rendalo_db` container including the database name.

```bash
# .env.dev

DATABASE_URL=mongodb://localhost:27017/ads-dev
```

```bash
# .env.test

DATABASE_URL=mongodb://localhost:27017/ads-test
```

## Up and Running

### Run

```bash
yarn local
```

### Routes

```bash
GET
http://localhost:8000/api/v1/ads

POST
http://localhost:8000/api/v1/ad
```

## Testing

### Unit Tests

```bash
yarn test:unit
```

### Integration Tests

```bash
yarn test:integration
```
