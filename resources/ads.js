const adsController = require('../controllers/ads');

/**
 * The resource to handle the getAds contract.
 *
 * @param   {ExpressRequest}    _           -- The express HTTP request.
 * @param   {ExpressResponse}   response    -- The express HTTP response.
 * @returns {void}
 */
async function getAds(_, response) {
  try {
    const collection = await adsController.getAll();
    
    response.json(collection);
  } catch (error) {
    response
      .status(500)
      .json({ message: error.message });
  }
}

/**
 * The resource to handle the addAd contract.
 *
 * @param   {ExpressRequest}    request     -- The express HTTP request.
 * @param   {ExpressResponse}   response    -- The express HTTP response.
 * @returns {void}
 */
async function addAd(request, response) {
  const { body } = request;
  const payload = {
    description: body.description,
    name: body.name,
    rent: body.rent,
  };

  try {
    await adsController.add(payload);
    
    response
      .status(201)
      .send();
  } catch (error) {
    response
      .status(400)
      .json({ message: error.message });
  }
}

module.exports = {
  addAd,
  getAds,
};