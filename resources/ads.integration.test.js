const test = require('ava');
const request = require('supertest');
const app = require('../index');
const Ad = require('../models/ad');

test.beforeEach(async () => {
  await Ad.deleteMany();
});

test.serial('GET /ads - retrieving zero ads', async t => {
  await Ad.deleteMany();

  const response = await request(app)
    .get('/api/v1/ads')
    .expect('Content-Type', /json/);

  t.is(response.status, 200);
  t.is(response.body.length, 0);
});

test.serial('GET /ads - retrieving some ads', async t => {
  await Ad.insertMany([
    {
      description: 'description 1',
      name: 'name 1',
      rent: 1000,
    }, {
      description: 'description 2',
      name: 'name 2',
      rent: 2000,
    },
  ]);

  const response = await request(app)
    .get('/api/v1/ads')
    .expect('Content-Type', /json/);

  t.is(response.status, 200);
  t.is(response.body.length, 2);
});

test.serial('POST /ad - missing required fields', async t => {
  const payload = { 
    name: 'name',
  };
  const response = await request(app)
    .post('/api/v1/ad')
    .send(payload);

  t.is(response.status, 400);
});

test.serial('POST /ad - created successfully', async t => {
  const payload = { 
    description: 'description',
    name: 'name',
    rent: 10000, 
  };
  const response = await request(app)
    .post('/api/v1/ad')
    .send(payload);
  const ad = await Ad.findOne({ name: payload.name });

  t.is(response.status, 201);
  t.is(ad.name, payload.name);
  t.is(ad.description, payload.description);
  t.is(ad.rent, payload.rent);
});
