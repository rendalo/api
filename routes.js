const express = require('express');
const router = express.Router();
const adsResources = require('./resources/ads');

/**
 * Getting all ads route.
 */
router.get('/ads', adsResources.getAds);

/**
 * Creating a new ad route.
 */
router.post('/ad', adsResources.addAd);

module.exports = router;