const test = require('ava');
const sinon = require('sinon');
const mongoose = require('mongoose');
const adsController = require('../controllers/ads');
const sandbox = sinon.createSandbox();
let getAllStub;
let addStub;

test.beforeEach(() => {
  getAllStub = sandbox.stub(mongoose.Model, 'find');
  addStub = sandbox.stub(mongoose.Model.prototype, 'save');
});

test.afterEach.always(() => {
  sandbox.restore();
});

test('Retrieving all available ads', async t => {
  const payload = [
    { 
      _id: '5d9b786b83b1df77bba90603',
      description: 'description 1',
      name: 'name 1',
      rent: 1000,
    }, {
      _id: '5d9b786b83b1df77bba90604',
      description: 'description 2',
      name: 'name 2',
      rent: 2000,
    },
  ];
  getAllStub.resolves(payload);

  const result = await adsController.getAll();

  t.deepEqual(result, payload);
  sinon.assert.calledOnce(mongoose.Model.find);
});

test.serial('Creating ad fails because missing required fields', async t => {
  const payload = { name: 'name' };

  addStub.throws();

  try {
    await adsController.add(payload);
  } catch (error) {
    sandbox.assert.calledOnce(mongoose.Model.prototype.save);
    t.is(error.name, 'Error');
  }
});

test.serial('Create a new ad successfully', async t => {
  const payload = { name: 'name', description: 'description', rent: 10000 };

  addStub.resolves(payload);

  await adsController.add(payload);

  sinon.assert.calledOnce(mongoose.Model.prototype.save);
  t.pass();
});
