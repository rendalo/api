const Ad = require('../models/ad');

/**
 * To get all the available ads.
 *
 * @returns {Ad[]}    A collection of ads.
 */
async function getAll() {
  try {
    const ads = await Ad.find();
    
    return ads;
  } catch (error) {
    throw error;
  }
}

/**
 * To create a new Ad.
 *
 * @param   {Ad}      payload   -- The payload to create the new ad.
 * @returns {void}
 */
async function add(payload) {
  const ad = new Ad(payload);

  try {
    await ad.save();
    
    return true;
  } catch (error) {
    throw error;
  }
}

module.exports = {
  add,
  getAll,
};