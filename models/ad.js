const mongoose = require('mongoose');

const adsSchema = new mongoose.Schema({
  createdAt: {
    default: Date.now,
    required: true,
    type: Date,
  },
  description: {
    required: true,
    type: String,
  },
  name: {
    required: true,
    type: String,
  },
  rent: {
    required: true,
    type: Number,
  },
});

module.exports = mongoose.model('Ad', adsSchema);